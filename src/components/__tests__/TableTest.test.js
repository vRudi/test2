import React from 'react'
import { shallow } from 'enzyme'
import renderer from 'react-test-renderer'
import TableTest from '../Table/TableTest'
import TableWrapper from '../Table/TableWrapper'

const setup = (props = {}) => {
  const component = shallow(<TableTest {...props} />)
  return component
}

describe('Table component', () => {
  it('should render without errors', () => {
    const component = shallow(<TableTest />)
    expect(component).toBeTruthy()
  })
  it('renders correctly', () => {
    const tree = renderer.create(<TableWrapper />).toJSON()
    console.log(tree)
    expect(tree).toMatchSnapshot()
  })
})
