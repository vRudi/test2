import React, { Component } from 'react'

export default class NotFound extends Component {
  // constructor(props) {
  //   super(props)

  // }
  render() {
    const { subtitle, paragraph } = this.props
    return (
      <div data-test="not-found">
        <h1 data-test="not-found__message">Page not found</h1>
        {subtitle && paragraph && (
          <div>
            <h2 data-test="not-found__subtitle">{subtitle}</h2>
            <p data-test="not-found__paragraph">{paragraph}</p>
          </div>
        )}
      </div>
    )
  }
}
