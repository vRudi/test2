import axios from 'axios'

// export const add = (a, b) => a + b
// export const total = (shipping, subtotal) => add(shipping, subtotal)
// export const nully = () => null
// export const weight = () => 800
// export const fetchUser = async id => {
//   const { data } = await axios.get(
//     `https://jsonplaceholder.typicode.com/users/${id}`
//   )
//   return data
// }
// export const user = () => ({
//   userName: 'Bob',
// })

// expect(chunk([1, 2, 3, 4, 5, 6])).toEqual([[1, 2], [3, 4], [5, 6]])
export const chunk = (arr, chunk) => {
  let counter = 0
  let newArr = []
  let tempArray = []

  arr.forEach(num => {
    if (counter < chunk) {
      tempArray.push(num)
      counter = counter + 1
    }
    if (counter === chunk) {
      newArr.push(tempArray)
      tempArray = []
      counter = 0
    }
  })
  return newArr
}
