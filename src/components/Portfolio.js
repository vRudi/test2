import React, { Component } from 'react'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table'

const table = {
  headers: ['Name', 'Alive'],
  rows: [
    { id: 1, name: 'John Smith', alive: false },
    { id: 2, name: 'Another name', alive: true },
    { id: 3, name: 'Person Personar', alive: true },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
    { id: 4, name: 'Sir Woodhouse', alive: false },
  ],
}

export default class Portfolio extends Component {
  render() {
    console.log(this.props)
    return (
      <Table multiSelectable fixedHeader selectable>
        <TableHeader>
          <TableRow>
            {table.headers.map((header, i) => (
              <TableHeaderColumn key={i}>{header}</TableHeaderColumn>
            ))}
          </TableRow>
        </TableHeader>

        <TableBody>
          {table.rows.map(({ id, name, alive }) => (
            <TableRow key={id}>
              <TableRowColumn>{name}</TableRowColumn>
              <TableRowColumn>{alive ? 'Yes' : 'No'}</TableRowColumn>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    )
  }
}
