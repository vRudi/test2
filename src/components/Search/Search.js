/**
 * Quick search functionality
 * Also blocks API calls on unmount
 * Fetches more on scroll
 */

import React, { Component } from 'react'
import { Avatar, Chip } from 'material-ui'

export default class Search extends Component {
  constructor(props) {
    super(props)
    this.state = {
      items: [],
      foundItems: [],
      start: 0,
      loading: false,
      mounted: false,
    }
    this.settings = {
      fetchLimit: 20,
    }
  }

  componentDidMount() {
    this.setState({ mounted: true })
    this.getData(0)
    window.addEventListener('scroll', this.handleScroll)
    this.searchBar.focus()
  }

  componentWillUnmount() {
    // Makes sure that state wont gete potentially unfinished API call
    this.setState({ mounted: false })
    window.removeEventListener('scroll', this.handleScroll)
  }

  handleScroll = () => {
    const windowHeight = window.innerHeight + document.documentElement.scrollTop
    const pageHeight = document.documentElement.offsetHeight

    if (!this.state.loading && windowHeight === pageHeight) {
      setTimeout(() => {
        this.getData(this.state.start)
      }, 1000)
    } else {
      this.setState({ loading: false })
    }
  }

  async getData(start) {
    const { fetchLimit } = this.settings
    this.setState({ loading: true })

    const res = await fetch(
      `https://jsonplaceholder.typicode.com/todos?_start=${start}&_limit=${fetchLimit}`
    )
    const data = await res.json()

    if (this.state.mounted) {
      this.setState(prevState => {
        return {
          items: [...prevState.items, ...data],
          foundItems: [...prevState.foundItems, ...data],
          start: this.state.start + fetchLimit,
          loading: false,
        }
      })
    }
  }

  handleSearch = e => {
    const { items } = this.state
    const search = e.target.value

    if (search.length > 0) {
      this.setState(prevState => {
        // Returns items that matches search phrase (case insensitive)
        const filtered = items.filter(item => {
          const toFind = item.title.toLowerCase()
          const searchTerm = search.toLowerCase()
          return toFind.includes(searchTerm)
        })

        return {
          ...prevState,
          foundItems: filtered,
        }
      })
    } else {
      this.setState({ foundItems: items })
    }
  }

  // Searchbar ref to focus in CDM
  setRef = node => (this.searchBar = node)
  render() {
    const { loading, foundItems, search } = this.state

    return (
      <div>
        <label htmlFor="search">Search</label>
        <input
          id="search"
          type="text"
          value={search}
          onChange={this.handleSearch}
          ref={this.setRef}
        />
        {foundItems && <p>Count: {foundItems.length}</p>}
        {foundItems &&
          foundItems.map(({ id, title }) => (
            <Chip key={id} style={{ margin: 5, display: 'inline-block' }}>
              <Avatar>{id}</Avatar>
              {title}
            </Chip>
          ))}
        {loading && <h1>Fetching more...</h1>}
      </div>
    )
  }
}
