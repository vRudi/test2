import React, { Component } from 'react'
import axios from 'axios'
import {
  CircularProgress,
  Paper,
  Snackbar,
  SelectField,
  MenuItem,
} from 'material-ui'

export default class About extends Component {
  constructor(props) {
    super(props)
    this.state = {
      person: {},
      ready: false,
      error: {
        exists: false,
        message: '',
      },
    }
  }

  componentDidMount() {
    this.getPerson(2)
  }

  getPerson = async id => {
    const isURL = string => /https:/i.test(string)
    // const TIMEOUT = 10

    try {
      this.setState({ ready: false })
      const res = await axios.get(`https://swapi.co/api/people/${id}`)
      if (res.status !== 200) throw Error("Couldn't find person :(")

      const person = res.data

      for (const prop in person) {
        // IF LIST RECEIVED IS ARRAY
        if (Array.isArray(person[prop])) {
          // Contains promises received from found URLs
          const promises = person[prop].map(url =>
            isURL(url) ? fetch(url) : url
          )

          const responses = await Promise.all(promises)
          const data = await responses.map(res => res.json())
          const personDetailGroup = await Promise.all(data)
          // Resolve and replace original URL string property to object returned from promise (from that URL)
          person[prop] = personDetailGroup
          this.setState({ person, ready: true })
        }
        // IF LIST RECEIVED IS A STRING
        if (typeof person[prop] === 'string' && isURL(person[prop])) {
          const res = await fetch(person[prop])
          const data = await res.json()
          person[prop] = data
        }
      }
    } catch (err) {
      this.setState({
        ready: true,
        error: { exists: true, message: err.message || 'Oops :(' },
      })
    }
  }

  render() {
    const { name, films } = this.state.person
    const { ready, error } = this.state
    const loader = <CircularProgress />

    return (
      <div style={{ margin: 10 }}>
        <Paper style={{ padding: '50px 30px' }} zDepth={1}>
          {ready ? (
            <div>
              <SelectPerson name={name} getPerson={this.getPerson} />
              <h3>{name} has these films:</h3>
              <ul>
                {films.map((film, i) => {
                  return (
                    <li key={i}>
                      {film.title} ({film.release_date})
                    </li>
                  )
                })}
              </ul>
            </div>
          ) : (
            loader
          )}
        </Paper>
        <Snackbar
          open={this.state.error.exists}
          message={error.message}
          autoHideDuration={4000}
          onRequestClose={this.handleRequestClose}
        />
      </div>
    )
  }
}

class SelectPerson extends Component {
  getItems = amount => {
    const items = []
    for (let i = 1; i < amount + 1; i++) {
      items.push(
        <MenuItem
          value={i}
          key={i}
          primaryText={i}
          onClick={() => this.props.getPerson(i)}
        />
      )
    }
    return items
  }
  render() {
    return <SelectField value={'choose'}>{this.getItems(100)}</SelectField>
  }
}
