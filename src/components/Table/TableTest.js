import React, { Component } from 'react'
import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui'

const capitalize = string => {
  if (typeof string !== 'string') return ''
  return string.charAt(0).toUpperCase() + string.slice(1)
}

export default class TableTest extends Component {
  state = {
    selectedRows: [],
  }

  getColumnHeaders = () => {
    const { people = [] } = this.props
    const { length } = people

    let noneFound = false
    const headers = length > 0 ? Object.keys(...people) : (noneFound = true)

    if (noneFound) {
      return <TableHeaderColumn>None Found</TableHeaderColumn>
    }

    return headers.map((header, i) => (
      <TableHeaderColumn className={`col-${header}`} key={header}>
        {capitalize(header)}
      </TableHeaderColumn>
    ))
  }

  getRowContents = () => {
    const { people } = this.props
    if (people) {
      return this.props.people.map(person => {
        const personDetail = Object.values(person)
        const personKeys = Object.keys(person)

        return (
          <TableRow key={person.id} selected={this.handleSelected(person.id)}>
            {personDetail.map(detail => {
              const detailIndex = personDetail.indexOf(detail)
              const columnName = personKeys[detailIndex]

              return (
                <TableRowColumn className={`col-${columnName}`} key={detail}>
                  {detail}
                </TableRowColumn>
              )
            })}
          </TableRow>
        )
      })
    }
  }

  handleRowSelect = index => {
    this.setState({ selectedRows: index })
  }

  handleSelected = index => {
    return this.state.selectedRows.indexOf(index) !== -1
  }

  render() {
    return (
      <div style={{ margin: 20 }}>
        <div>
          <Table multiSelectable={true} onRowSelection={this.handleRowSelect}>
            <TableHeader>
              <TableRow>{this.getColumnHeaders()}</TableRow>
            </TableHeader>

            <TableBody>{this.getRowContents()}</TableBody>
          </Table>
        </div>
      </div>
    )
  }
}
