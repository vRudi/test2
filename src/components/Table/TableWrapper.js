import React, { Component } from 'react'
import people from '../db'
import TableTest from './TableTest'
import TableSearch from './TableSearch'

export default class TableWrapper extends Component {
  peopleKeys = people.length > 0 ? Object.keys(...people) : []

  state = {
    columnsToDisplay: this.peopleKeys,
    hiddenColumns: [],
  }

  setColumnsToDisplay = columnsToDisplay => {
    this.setState({ columnsToDisplay })
  }

  render() {
    const { columnsToDisplay } = this.state
    return (
      <div>
        <TableSearch
          people={people}
          setColumnsToDisplay={this.setColumnsToDisplay}
          columnsToDisplay={columnsToDisplay}
        />
        <TableTest people={people} columnsToDisplay={columnsToDisplay} />
      </div>
    )
  }
}
