import React, { Component } from 'react'
import { Toolbar, ToolbarGroup, MenuItem, SelectField } from 'material-ui'

export default class TableSearch extends Component {
  handleChange = (event, index, selectedColumns) => {
    // Sending info to TableTest
    this.props.setColumnsToDisplay(selectedColumns)

    // Toggling visibility in the actula table
    const hiddenColumn = event.target.innerText
    document.querySelectorAll(`.col-${hiddenColumn}`).forEach(column => {
      if (column.style.display === 'none') {
        column.style.display = 'table-cell'
      } else {
        column.style.display = 'none'
      }
    })
  }

  availableColumns = values => {
    const { people, columnsToDisplay } = this.props
    const { length } = people

    if (people && columnsToDisplay) {
      const availableColHeaders = length > 0 ? Object.keys(...people) : []

      return availableColHeaders.map(header => (
        <MenuItem
          key={header}
          insetChildren={true}
          value={header}
          primaryText={header}
          checked={values && columnsToDisplay.includes(header)}
        />
      ))
    }
  }

  render() {
    const { columnsToDisplay } = this.props

    return (
      <Toolbar>
        <ToolbarGroup>
          <SelectField
            hintText="Columns to show"
            multiple={true}
            value={columnsToDisplay}
            onChange={this.handleChange}
          >
            {this.availableColumns(columnsToDisplay)}
          </SelectField>
        </ToolbarGroup>
      </Toolbar>
    )
  }
}
