import React, { Component } from 'react'
import { AppBar, FlatButton } from 'material-ui'

// const pages = [
//   {
//     id: 0,
//     name: 'Home',
//     options: {
//       href: '/',
//       exact: true,
//       activeClassName: 'active',
//     },
//   },
//   {
//     id: 1,
//     name: 'About',
//     options: {
//       href: '/about',
//       activeClassName: 'active',
//     },
//   },
//   {
//     id: 2,
//     name: 'Components',
//     options: {
//       href: '/search',
//       activeClassName: 'active',
//     },
//   },
// ]

// const navigation = pages.map(page => {
//   return <FlatButton key={page.id} label={page.name} href={page.options.href} />
// })

export default class Header extends Component {
  render() {
    return (
      <AppBar
        title="Website"
        // iconElementRight={navigation}
        iconElementRight={
          <span>
            <FlatButton label="Save" />
          </span>
        }
      />
    )
  }
}
