import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import './App.css'

// Routes
import NotFound from './components/NotFound'
import Header from './components/Header'
import Home from './components/Home'
import Portfolio from './components/Portfolio'
import Search from './components/Search'
import TableWrapper from './components/Table/TableWrapper'

export default class App extends Component {
  render() {
    return (
      <MuiThemeProvider>
        <Router>
          <main>
            <Header />
            <Switch>
              <Route path="/" exact component={Home} />
              <Route path="/table" exact component={TableWrapper} />
              <Route path="/about" component={Portfolio} />
              <Route path="/search" component={Search} />
              <Route path="/portfolio/:id" exact component={Portfolio} />
              <Route component={NotFound} />
            </Switch>
          </main>
        </Router>
      </MuiThemeProvider>
    )
  }
}
